var Editor = (function($, HBS, M, R) {
	var _container = null;
	var _currentPos = 0;
	var _maxWidth = 0;
	var _isOpened = false;
	var _haveAnimations = false;
	var _presets = false;
	var _editorElement = null;
	var _currentImage = null;
	var _lightnessPicker = null;
	var _inputField = null;
	var _cursorPosition = 0;
	var _slideNext = function(cb) {
		if (_currentPos === _maxWidth - 240) {
			return;
		}
		if (cb && typeof cb == 'function') {
			setTimeout(cb, 500);
		}
		_currentPos += 240;
		var currentScreen = _container.find(".screen.animate");
		currentScreen.removeClass('animate');
		currentScreen.next('.screen').addClass('animate');
		if (_haveAnimations) {
			_container.find("#slider").css('marginLeft', '-=240px');
			return;
		}
		_container.find("#slider").animate({
			marginLeft: '-=240px'
		}, 500);
	};
	var _slidePrevious = function(cb) {
		if (_currentPos === 0) {
			return;
		}
		if (cb && typeof cb == 'function') {
			setTimeout(cb, 500);
		}
		_currentPos -= 240;
		var currentScreen = _container.find(".screen.animate");
		currentScreen.removeClass('animate');
		currentScreen.prev('.screen').addClass('animate');
		if (_haveAnimations) {
			_container.find("#slider").css('marginLeft', '+=240px');
			return;
		}
		_container.find("#slider").animate({
			marginLeft: '+=240px'
		}, 500);
	};
	var _reset = function() {
		_container.find('.headline').removeClass('animate');
		_container.find('#slider').css('marginLeft', 0);
	};
	var _onPresetsLoaded = function(presets) {
		_presets = presets;
	};
	var _renderPresets = function() {
		var img = $('<img>');
		img.attr('src', 'uploads/' + _currentImage);
		img.css('display', 'none');
		$("body").append(img);
		img.load(_onImageLoaded);
	};
	var _onImageLoaded = function() {
		var accumulator = "";
		var t = HBS.templates['editor-preset'];
		var params = {};
		for (var p in _presets) {
			params = _presets[p];
			params.id = p;
			params.image = _currentImage;
			accumulator += t(params);
		}
		_container.find('.font-list').find("#list").html(accumulator);
		_container.find('.font-list').find('.card').on('click', _onPresetSelected);
		_container.find('.font-list').find("#list").niceScroll();
		_slideNext();
	};
	var _onPictureSelected = function() {
		var iframe = $('<iframe name="postiframe" id="postiframe" style="display: none" />');
		$("body").append(iframe);
		var form = $(this).parent();
		form.attr('target', 'postiframe');
		iframe.load(function() {
			var response = $.parseJSON($($("#postiframe")[0].contentWindow.document.body.innerHTML).html());
			$("#postiframe").remove();
			if (response.error) {
				alert(response.error);
				return;
			}
			_currentImage = response.result;
			_renderPresets();
		});
		form.submit();
	};
	var _onPresetSelected = function() {
		_cursorPosition = 0;
		var presetId = $(this).attr('data-id');
		_container.find('.creation').find('#canvas').off('click');
		var preset = _presets[presetId];
		R.initWithParams({
			imageURL: _currentImage,
			preset: preset,
			container: _container.find("#canvas")
		});
		R.render();
		_container.find('.creation').find('.color-selector').colorpicker({
			delegate: Editor
		});
		_lightnessPicker = _container.find('.creation').find('.lightness-selector').colorpicker({
			delegate: Editor,
			hasBackground: true,
			background: 'img/colorpicker-lightness.png',
		});
		_container.find('.creation .toggle-drawing').unbind();
		_container.find('.creation').find('#canvas').on('click', _setFirstResponder);
		_container.find('.creation').find("textarea").unbind();
		_inputField = _container.find('.creation').find("textarea");
		_inputField.attr('placeholder', preset.label);
		_inputField.on('keyup', _onTextChanged);
		_inputField.on('change', _onTextChanged);
		_inputField.on('paste', _onTextChanged);
		_inputField.on('focusout', _unsetFirstResponder);
		_container.find('.creation .toggle-drawing').attr('checked', false);
		_container.find('.creation .toggle-drawing').on('click', _onToggleDrawingMode);
		_slideNext();
	};
	var _onToggleDrawingMode = function() {
		R.toggleDrawingMode();
	};
	var _setFirstResponder = function() {
		// if (_isEditingText) return;
		// _isEditingText = true;
		_inputField.focus();
		var text = $.trim(_inputField.val());
		R.willStartTextEditing(text.length);
		// window.requestAnimFrame(_renderCursor);
	};
	var _unsetFirstResponder = function() {
		R.willEndTextEditing();
	};
	var _onTextChanged = function(e) {
		var text = "";
		if (e.keyCode == 37) {
			_cursorPosition--;
			R.setCursorPosition(_cursorPosition);
			return;
		}
		if (e.keyCode == 39) {
			_cursorPosition++;
			R.setCursorPosition(_cursorPosition);
			return;
		}
		if (e.keyCode == 8) {
			_cursorPosition--;
			R.setCursorPosition(_cursorPosition);
			text = $.trim($(this).val());
			Renderer.setText(text);
			return;
		}
		if(e.keyCode != 32 && e.keyCode != 13) {
			_cursorPosition++;
		}
		R.setCursorPosition(_cursorPosition);
		text = $(this).val();
		R.setText(text);
	};
	return {
		init: function() {
			_container = $('section[role=editor]');
			_container.find('.headline .back').on('click', _slidePrevious);
			_container.find('.capture button').on('click', _slideNext);
			_container.find('#picture').on('change', _onPictureSelected);
			if (M.cssanimations) {
				_haveAnimations = true;
			}
			if (!_presets) {
				$.get('js/presets.json', _onPresetsLoaded);
			}
			_currentImage = '13f53d05476cff3b0909d05b522ec4a851fd3b22.jpg';
			_renderPresets();
		},
		getLaunchButton: function() {
			var t = HBS.templates['editor-launch-button'];
			return t({
				state: _isOpened
			});
		},
		toggleLaunchButton: function() {
			_isOpened = !_isOpened;
			return this.getLaunchButton();
		},
		getEditor: function() {
			if (_editorElement) {
				return _editorElement;
			}
			var t = HBS.templates['editor-container'];
			var el = $(t());
			_maxWidth = el.find('.screen').length * 240;
			el.find("#slider").width(_maxWidth + "px");
			_editorElement = el[0].outerHTML;
			return _editorElement;
		},
		slideNext: function() {
			return _slideNext();
		},
		slidePrevious: function() {
			_slidePrevious();
		},
		reset: function() {
			_reset();
		},
		onColorChanged: function(e) {
			if (e.currentTarget.hasClass('lightness-selector')) {
				R.setColor(e.color);
				return;
			}
			_lightnessPicker.setBackgroundColor(e.color);
		}
	};
})(jQuery, Handlebars, Modernizr, Renderer);