var Site = (function(M, $, HBS, E){
	var _container = null;
	var _canvasIsSupported = false;
	var _subscriberCount = 0;
	var _referer;
	var _position = false;
	var _getPosition = function() {
		if(_position) {
			return _position;
		}
		var c = document.cookie;
		if (/jestr-position/g.test(c)) {
			var j = c.indexOf('jestr-position') + 15;
			var sc = c.substr(j);
			sc = (sc.indexOf(';') > 0) ? sc.substr(0, sc.indexOf(';')) : sc;
			_position = Number(sc);
			return _position;
		}
		return false;
	};
	var _onEditorLaunchButtonClick = function() {
		var el = $(E.toggleLaunchButton());
		_container.find("#launch").html(el.html());
		_container.find("#background-wrapper").toggleClass('overlay');
		_container.find("section[role=editor]").toggleClass('show');
		setTimeout(function(){
			_container.find("section[role=editor] .capture").toggleClass('show');
		}, 10);
	};
	var _renderIndexScreen = function() {
		var launchButton = (_canvasIsSupported) ? E.getLaunchButton() : "";
		var editor = (_canvasIsSupported) ? E.getEditor() : "";
		var t = HBS.templates['main-index'];
		var params = {
			referer: _referer,
			launchButton: launchButton,
			editor: editor
		};
		var position = _getPosition();
		if(position) {
			params.position = position;
			params.difference = _subscriberCount - position;
		}
		var el = $(t(params));
		_container.html(el);
		_container.find("#launch").on('click', _onEditorLaunchButtonClick);
		emojify.setConfig({
			img_dir: 'img/emoji',
		});
		emojify.run(_container[0]);
		Editor.init();
		_container.find("section[role=subscribe]").find("form").on('submit', _onSubscriptionSubmit);
	};
	var _renderInivitationScreen = function() {
		var t = HBS.templates['main-invite'];
		_container.html(t({referer: _referer}));
		emojify.setConfig({
			img_dir: 'img/emoji',
		});
		emojify.run(_container[0]);
		_container.find("section[role=invitation]").find("form").on('submit', _onInvitationSubmit);
	};
	var _onSubscriptionSubmit = function(e) {
		e.preventDefault();
		var form = $(this);
		$.ajax({
			method: 'post',
			url: form.attr('action'),
			data: form.serialize(),
			dataType: 'json',
			cache: false,
			success: function(r) {
				_position = Number(r.position);
				_renderIndexScreen();
			}
		});
	};
	var _onInvitationSubmit = function(e) {
		e.preventDefault();
		var form = $(this);
		$.ajax({
			method: 'post',
			url: form.attr('action'),
			data: form.serialize(),
			dataType: 'json',
			cache: false,
			success: function(r) {
				if(r.status === 'ok') {
					alert('We\'ve sent out your invitations, thank you! :)');
					_renderIndexScreen();
					return;
				}
			}
		});
	};
	return {
		init: function(data) {
			if(registerHelpers && typeof registerHelpers == 'function') {
			    registerHelpers([this, Editor, Renderer]);
			}
			_container = $("main");
			if(M.canvas && M.canvastext) {
				_canvasIsSupported = true;
			}
			_referer = data.params.referer || false;
			if(data.params.method && data.params.method === 'invitation') {
				_renderInivitationScreen();
				return;
			}
			_subscriberCount = data.subscriberCount || 0;
			_renderIndexScreen();
		}
	};
})(Modernizr, jQuery, Handlebars, Editor);