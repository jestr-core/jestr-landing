var Renderer = (function(d, $) {
	var _canvas;
	var _canvasContainer = null;
	var _currentContext = null;

	var _contextStateCache = [];

	var _canvasOffset = null;

	var _currentImage = null;
	var _currentPreset = null;

	var _isDrawingMode = false;

	var _currentColor = "#000";

	var _drawingStarted = false;
	var _movingStarted = false;

	var _startPoint = {};

	var _pointCache = [];
	var _pointsLayer = [];

	var _textFormat = {
		"font": "Arial",
		"size": "20px",
		"background": false,
		"color": "#000"
	};

	var _currentStrokeWidth = 10;

	var _dimensions = {
		width: 240,
		height: 240
	};
	var _lastCenter = null;
	var _center = {
		x: _dimensions.height >> 1,
		y: _dimensions.width >> 1
	};

	var _currentAngle = 0;
	var _currentScale = 1;

	var _cursorPosition = 0;
	var _isTextEditing = false;
	var _cursorTimer = 0;
	var _showCursor = false;
	var _cursorClip = null;

	var _currentTextBoundaries = [];

	var _parseStyle = function() {
		var parts = _currentPreset.style.split(';');
		var styles = [];
		for (var p in parts) {
			var i = parts[p].split(':');
			styles.push(i[0]);
			styles.push(i[1]);
		}
		_textFormat.font = styles[styles.indexOf('font-family') + 1];
		_textFormat.size = styles[styles.indexOf('font-size') + 1];
		_textFormat.color = styles[styles.indexOf('color') + 1];
		_textFormat.background = (styles.indexOf('background') !== -1) ? styles[styles.indexOf('background') + 1] : false;
		_isDrawingMode = false;
		_drawingStarted = false;
		_pointCache = [];
	};
	var _createElement = function() {
		_canvasContainer.html('');
		_canvas = d.createElement('canvas');
		_canvas.width = _dimensions.width;
		_canvas.height = _dimensions.height;
		_currentContext = _canvas.getContext("2d");
		_canvasContainer.append(_canvas);
	};
	var _makeTempContextWithScale = function(scale) {
		_canvas = $("<canvas>")[0];
		_canvas.width = _dimensions.width * scale;
		_canvas.height = _dimensions.height * scale;
		_currentContext = _canvas.getContext("2d");
	};
	var _scaleUpLayers = function(scale) {
		_dimensions.width = _dimensions.width * scale;
		_dimensions.height = _dimensions.height * scale;
		_center.x = _center.x * scale;
		_center.y = _center.y * scale;
		_textFormat.size = (parseInt(_textFormat.size, 10) * scale) + "px";
		var currentLayer;
		for (var i = 0;  i < _pointsLayer.length; i++) {
			currentLayer = _pointsLayer[i];
			currentLayer.strokeWidth = currentLayer.strokeWidth * scale;
			for (var j = currentLayer.points.length - 1; j >= 0; j--) {
				currentLayer.points[j].x = currentLayer.points[j].x * scale;
				currentLayer.points[j].y = currentLayer.points[j].y * scale;
			}
		}
	};
	// event handlers
	var _onMouseDown = function(e) {
		_canvasOffset = canvas.getBoundingClientRect();
		_startPoint = {
			x: e.pageX - _canvasOffset.left,
			y: e.pageY - _canvasOffset.top
		};
		if (_isDrawingMode) {
			_renderBackground();
			_renderDrawing();
			_drawingStarted = true;
			_pointCache = [];
			var newColor = _currentColor;
			var newStrokeWidth = _currentStrokeWidth;
			_pointsLayer.push({
				color: newColor,
				strokeWidth: newStrokeWidth,
				points: [_startPoint]
			});
			return;
		}
		_movingStarted = true;
		_lastCenter = _center;
	};
	var _onMouseUp = function(e) {
		if (_isDrawingMode) {
			_onDrawingEnded();
			return;
		}
		_onMovingEnded();
	};
	var _onMouseMove = function(e) {
		if (_isDrawingMode) {
			if (_drawingStarted) {
				_pointCache.push({
					x: e.pageX - _canvasOffset.left,
					y: e.pageY - _canvasOffset.top
				});
				setTimeout(_drawTrace, 1);
			}
			return;
		}
		if (_movingStarted) {
			// todo: calc bouding rect of text
			var newX = _lastCenter.x + ((e.pageX - _canvasOffset.left) - _startPoint.x);
			var newY = _lastCenter.y + ((e.pageY - _canvasOffset.top) - _startPoint.y);
			_center = {
				x: (newX < 0) ? 0 : (newX > _dimensions.height) ? _dimensions.height : newX,
				y: (newY < 0) ? 0 : (newY > _dimensions.width) ? _dimensions.width : newY
			};
			_render();
		}
	};
	var _onMouseOut = function(e) {
		_onMouseUp.call(this, e);
	};
	var _onDrawingEnded = function() {
		if (!(_isDrawingMode && _drawingStarted)) {
			return;
		}
		_drawingStarted = false;
		_render();
	};
	var _onMovingEnded = function() {
		_movingStarted = false;
		_render();
	};
	var _setupEventHandlers = function() {
		_canvasContainer.unbind();
		_canvasContainer.off('mousedown mouseup mouseout mousemove');
		_canvasContainer.on('mousedown', _onMouseDown);
		_canvasContainer.on('mouseup', _onMouseUp);
		_canvasContainer.on('mouseout', _onMouseOut);
		_canvasContainer.on('mousemove', _onMouseMove);
	};
	// rendering
	var _drawTrace = function() {
		var pos;
		var lastLayer = _pointsLayer[_pointsLayer.length - 1];
		while (pos = _pointCache.shift()) {
			lastLayer.points.push(pos);
			_drawLine(_startPoint, pos, lastLayer.color, lastLayer.strokeWidth);
			_startPoint = pos;
		}
	};
	var _drawLine = function(from, to, color, strokeWidth) {
		_currentContext.lineJoin = "round";
		_currentContext.lineWidth = strokeWidth;
		_currentContext.beginPath();
		_currentContext.moveTo(from.x, from.y);
		_currentContext.lineTo(to.x, to.y);
		_currentContext.closePath();
		_currentContext.strokeStyle = color;
		_currentContext.stroke();
	};
	var _renderDrawing = function() {
		var layer;
		for (var i = 0; i < _pointsLayer.length; i++) {
			layer = _pointsLayer[i];
			for (var j = layer.points.length - 1; j > 0; j--) {
				_drawLine(layer.points[j], layer.points[j - 1], layer.color, layer.strokeWidth);
			}
		}
	};
	var _calculateTextBoundaries = function() {
		var result = [];
		var lines = Renderer.text.split("\n");
		var linePos = {
			size: {
				height: parseInt(_textFormat.size, 10) * ((_currentPreset.lineMultiplicator) ? _currentPreset.lineMultiplicator : 1.2)
			},
			origin: {

			}
		};
		var offset = ((lines.length - 1) * linePos.size.height) >> 1;
		_currentContext.textBaseline = "middle";
		_currentContext.font = _textFormat.size + " " + _textFormat.font;
		for (var line in lines) {
			lines[line] = $.trim(lines[line]);
			if(lines[line].length === 0){
				break;
			}
			linePos.size.width = _currentContext.measureText(lines[line]).width;
			linePos.origin.x = _center.x - (linePos.size.width >> 1);
			if (lines.length > 1) {
				var row = line * linePos.size.height;
				linePos.origin.y = _center.y - offset + row;
			} else {
				linePos.origin.y = _center.y;
			}
			var lineData = {
				text: lines[line],
				position: linePos.copy()
			};
			result.push(lineData);
		}
		_currentTextBoundaries = result;
	};
	var _renderText = function() {
		_calculateTextBoundaries();
		var line;
		for (var i in _currentTextBoundaries) {
			line = _currentTextBoundaries[i];
			_currentContext.save();
			_currentContext.translate(_center.x, _center.y);
			_currentContext.rotate(_currentAngle * Math.PI / 180);
			_currentContext.scale(_currentScale, _currentScale);
			_currentContext.translate(-_center.x, -_center.y);
			if (_textFormat.background) {
				_currentContext.fillStyle = _textFormat.background;
				var bgSize = {
					width: line.position.size.width + _dimensions.width * 0.04,
					height: parseInt(_textFormat.size, 10) + _dimensions.height * 0.06
				};
				var bgPos = {
					x: line.position.origin.x - _dimensions.width * 0.02,
					y: line.position.origin.y - (parseInt(_textFormat.size, 10) >> 1) - _dimensions.height * 0.03
				};
				_currentContext.fillRect(bgPos.x, bgPos.y, bgSize.width, bgSize.height);
			}
			_currentContext.fillStyle = _textFormat.color;
			_currentContext.fillText(line.text, line.position.origin.x, line.position.origin.y + line.position.size.height * 0.1);
			_currentContext.restore();
		}
	};
	var _renderBackground = function() {
		_currentContext.drawImage(_currentImage, 0, 0, _dimensions.width, _dimensions.height);
	};
	var _render = function() {
		_renderBackground();
		_renderDrawing();
		_renderText();
	};
	var _renderCursor = function() {
		// BUGGY
		return;

		
		// if(_isTextEditing) {
		// 	window.requestAnimFrame(_renderCursor);
		// }
		// if(_cursorTimer > Math.round(+new Date()/1000)) {
		// 	return;
		// }
		// //console.log('Tick...');
		// _cursorTimer = Math.round(+new Date()/1000) + 1;
		// _showCursor = !_showCursor;
		// _currentContext.save();
		// _currentContext.translate(_center.x, _center.y);
		// _currentContext.rotate(_currentAngle * Math.PI / 180);
		// _currentContext.scale(_currentScale, _currentScale);
		// _currentContext.translate(-_center.x, -_center.y);
		// var i = 0;
		// var offset = _cursorPosition;
		// while(i < _currentTextBoundaries.length && _cursorPosition > _currentTextBoundaries[i].text.length) {
		// 	offset -= _currentTextBoundaries[i].text.length;
		// 	i++;
		// }
		// if(i == _currentTextBoundaries.length) {
		// 	console.log('WTFLOL');
		// 	return;
		// }
		// var startX = _currentContext.measureText( _currentTextBoundaries[i].text.substr(0, offset) ).width;
		// var textSize = parseInt(_textFormat.size, 10);
		// if(_showCursor && _isTextEditing) {
		// 	_currentContext.fillStyle = _textFormat.color;
		// 	_currentContext.fillRect(_currentTextBoundaries[i].position.origin.x + startX, _currentTextBoundaries[i].position.origin.y - (textSize >> 1), 2, textSize);
		// } else {
		// 	_currentContext.drawImage(_currentImage, 0, 0, _dimensions.width, _dimensions.height, _currentTextBoundaries[i].position.origin.x + startX, _currentTextBoundaries[i].position.origin.y - (textSize >> 1), 2, textSize);
		// }
		// _currentContext.restore();
	};
	var _pushContextState = function() {
		var lastCanvas = _canvas;
		var lastContext = _currentContext;
		var lastCenter = {
			x: _center.x,
			y: _center.y
		};
		var lastCanvasDimensions = {
			width: _dimensions.width,
			height: _dimensions.height
		};
		var lastTextFize = _textFormat.size;
		var lastPointsLayer = _pointsLayer.copy();
		var state = {
			canvas: lastCanvas,
			context: lastContext,
			center: lastCenter,
			dimensions: lastCanvasDimensions,
			textSize: lastTextFize,
			pointsLayer: lastPointsLayer
		};
		console.log(state);
		_contextStateCache.push(state);
	};
	var _popContextState = function() {
		var state = _contextStateCache.pop();
		console.log(state);
		_center = state.center;
		_dimensions = state.dimensions;
		_canvas = state.canvas;
		_textFormat.size = state.textSize;
		_pointsLayer = state.pointsLayer;
		_currentContext = state.context;
	};
	return {
		initWithParams: function(options /*imageURL, preset, container*/ ) {
			_currentPreset = options.preset;
			_canvasContainer = options.container || $("#canvas");
			_contextStateCache = [];
			_currentImage = $("<img>").attr('src', 'uploads/' + options.imageURL)[0];
			_isDrawingMode = false;
			_drawingStarted = false;
			_parseStyle();
			_dimensions = options.dimensions || {
				width: 240,
				height: 240
			};
			_center = {
				x: _dimensions.height >> 1,
				y: _dimensions.width >> 1
			};
			_cursorOrigin = _center;
			_currentScale = 1;
			_currentAngle = 0;
			_pointsLayer = [];
			_lastCenter = {};
			_currentStrokeWidth = 10;
			this.text = this.text || _currentPreset.label;
			_createElement();
			_setupEventHandlers();
			return this;
		},
		render: function() {
			_render();
		},
		willStartTextEditing: function(pos) {
			_cursorPosition = pos;
			_isTextEditing = true;
			_cursorTimer = Math.round(+new Date()/1000);
			_showCursor = true;
			window.requestAnimFrame(_renderCursor);
		},
		willEndTextEditing: function() {
			_cursorPosition = 0;
			_isTextEditing = false;
			window.cancelAnimFrame(_renderCursor);
		},
		setCursorPosition: function(pos) {
			_cursorPosition = pos;
		},
		text: false,
		setText: function(text) {
			if (_isDrawingMode) {
				return;
			}
			this.text = text;
			_render();
		},
		setColor: function(color) {
			_currentColor = color;
			if (_isDrawingMode) {
				return;
			}
			_textFormat.color = color;
			_render();
		},
		setAngle: function(angle) {
			_currentAngle = angle;
			_render();
		},
		setScale: function(scale) {
			if (scale >= 0.1 && scale <= 10) {
				_currentScale = scale;
				_render();
			}
		},
		setStrokeWidth: function(strokeWidth) {
			if(strokeWidth >= 3 && strokeWidth <= 50) {
				_currentStrokeWidth = strokeWidth;
			}
		},
		undo: function() {
			_pointsLayer.pop();
			_render();
		},
		addImage: function(image) {
			// todo: implement
		},
		toggleDrawingMode: function() {
			_isDrawingMode = !_isDrawingMode;
		},
		snapshot: function(scale) {
			this.willEndTextEditing();
			var multiplier = scale || 2;
			_pushContextState();
			_makeTempContextWithScale(multiplier);
			_scaleUpLayers(multiplier);
			_render();
			var snapshot = new Image();
			snapshot.src = _canvas.toDataURL('image/jpeg', 100);
			_popContextState();
			_render();
			return snapshot;
		}
	};
})(document, jQuery);