/* Helpers */
function registerHelpers(modules) {
	var functionFactory = {
		respondsToSelector: function(selector) {
			if (!selector) {
				throw new Error('respondsToSelector: "selector" parameter is missing.');
			}
			return (this.hasOwnProperty(selector) && typeof this[selector] === 'function');
		}
	};
	for (var m in modules) {
		for (var func in functionFactory) {
			modules[m][func] = functionFactory[func];
		}
	}
	CanvasRenderingContext2D.prototype.roundRect = function(x, y, w, h, r) {
		if (w < 2 * r) r = w / 2;
		if (h < 2 * r) r = h / 2;
		this.beginPath();
		this.moveTo(x + r, y);
		this.arcTo(x + w, y, x + w, y + h, r);
		this.arcTo(x + w, y + h, x, y + h, r);
		this.arcTo(x, y + h, x, y, r);
		this.arcTo(x, y, x + w, y, r);
		this.closePath();
		return this;
	};
}

Object.defineProperty(Object.prototype, "copy", {
	enumerable: false,
	writable: true,
	value: function(testFun) {
		var out, v, key;
		out = Array.isArray(this) ? [] : {};
		for (key in this) {
			if(this.hasOwnProperty(key)) {
				v = this[key];
				out[key] = (typeof v === "object") ? v.copy() : v;
			}
		}
		return out;
	}
});