<?php

    if(isset($_GET['reset']))
        setcookie('jestr-position',"");

?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>jestr</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <meta name="description" content="We think text messages are super boring. Jestr is coming to expose your creativity.">
        <meta property="description" content="We think text messages are super boring. Jestr is coming to expose your creativity.">
        <meta property="og:type"   content="website" /> 
        <meta property="og:url"    content="http://jestr.me" /> 
        <meta property="og:title"  content="jestr" /> 
        <meta property="og:image"  content="http://jestr.me/img/logo_200x200.png" /> 

        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
        <link rel="stylesheet" href="css/main.css">
        <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">

        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <header>
            <a id="logo" href="/">
                <img src="img/logo_70x70@2x.png">
                jestr
            </a>
            <div id="social">
                <a id="facebook" href="https://www.facebook.com/sharer/sharer.php?u=jestr.me" title="Share on Facebook" target="shareJestr"><img src="img/facebook.svg"></a>
                <a id="twitter" href="http://twitter.com/home?status=Jestr is a new, must have app for your iPhone, check out!%20http%3A%2F%2Fwww.jestr.me%20%23jestr" title="Share on Twitter" target="shareJestr"><img src="img/twitter.svg"></a>
                
            </div>
        </header>
        <main>
            <!-- content comes here -->
        </main>
        <footer>
            <ul>
                <li>
                    <a href="#">Terms &amp; Conditions</a>
                </li>
                <li>
                    <a href="#">Privacy Policy</a>
                </li>
                <li>
                    <a href="mailto:hello@jestr.me">Contact us</a>
                </li>
            </ul>
            <p>
                Copyright &copy; Peabo Media Ltd.
            </p>
        </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>

        <script>
            $(document).ready(function(){
                var subscriberCount = <?php 
                    $padding = 1420 + 46;
                    $link = mysql_connect("localhost","jestr","Ng]Z8[i@#twt/4~");
                    mysql_select_db("jestr", $link);
                    $query = "SELECT max(id) as count FROM subscriber";
                    $mq = mysql_query($query);
                    $data = mysql_fetch_assoc($mq);
                    echo $padding + $data['count'];
                ?>;
                Site.init({
                    subscriberCount: subscriberCount,
                    params: <?= json_encode($_GET) ?>
                });
            });
        </script>

        <!--<script src="js/main.min.js"></script>-->
        <script src="js/vendor/handlebars.runtime-v2.0.0.js"></script>
        <script src="js/src/helpers.js"></script>
        <script src="js/src/plugins.js"></script>
        <script src="js/src/tpl.js"></script>
        <script src="js/src/renderer.js"></script>
        <script src="js/src/editor.js"></script>
        <script src="js/src/main.js"></script>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-46299878-1', 'jestr.me');
          ga('send', 'pageview');

        </script>
    </body>
</html>
