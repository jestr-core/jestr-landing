<?php

header('Content-Type: application/json; charset=utf-8');

try {
   
    // Undefined | Multiple Files | $_FILES Corruption Attack
    // If this request falls under any of them, treat it invalid.
    if (
        !isset($_FILES['picture']['error']) ||
        is_array($_FILES['picture']['error'])
    ) {
        throw new RuntimeException('Invalid parameters.');
    }

    // Check $_FILES['picture']['error'] value.
    switch ($_FILES['picture']['error']) {
        case UPLOAD_ERR_OK:
            break;
        case UPLOAD_ERR_NO_FILE:
            throw new RuntimeException('No file sent.');
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            throw new RuntimeException('Exceeded filesize limit.');
        default:
            throw new RuntimeException('Unknown errors.');
    }

    // You should also check filesize here.
    if ($_FILES['picture']['size'] > 1024 * 1024 * 15) {
        throw new RuntimeException('Exceeded filesize limit.');
    }

    // DO NOT TRUST $_FILES['picture']['mime'] VALUE !!
    // Check MIME Type by yourself.
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    if (false === $ext = array_search(
        $finfo->file($_FILES['picture']['tmp_name']),
        array(
            'jpg' => 'image/jpeg',
            'png' => 'image/png',
        ),
        true
    )) {
        throw new RuntimeException('Invalid file format.');
    }

    // You should name it uniquely.
    // DO NOT USE $_FILES['picture']['name'] WITHOUT ANY VALIDATION !!
    // On this example, obtain safe unique name from its binary data.

    $dst = sprintf('%s.%s',
            sha1_file($_FILES['picture']['tmp_name']),
            'jpg');

    // if (!move_uploaded_file(
    //     $_FILES['picture']['tmp_name'],
    //     $dst
    // )) {
    //     throw new RuntimeException('Failed to move uploaded file.');
    // }

    switch ($ext) {
    	case 'jpg':
    		$image = imagecreatefromjpeg($_FILES['picture']['tmp_name']);
    		break;
    	case 'png':
    		$image = imagecreatefrompng($_FILES['picture']['tmp_name']);
    		break;
    	default:
    		 throw new RuntimeException('Failed to create image resource.');
    		break;
    }

	$thumb_width = 320;
	$thumb_height = 320;

	$width = imagesx($image);
	$height = imagesy($image);

	$original_aspect = $width / $height;
	$thumb_aspect = $thumb_width / $thumb_height;

	if ( $original_aspect >= $thumb_aspect )
	{
	   $new_height = $thumb_height;
	   $new_width = $width / ($height / $thumb_height);
	}
	else
	{
	   $new_width = $thumb_width;
	   $new_height = $height / ($width / $thumb_width);
	}

	$thumb = imagecreatetruecolor( $thumb_width, $thumb_height );

	// Resize and crop
	imagecopyresampled($thumb,
	                   $image,
	                   0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
	                   0 - ($new_height - $thumb_height) / 2, // Center the image vertically
	                   0, 0,
	                   $new_width, $new_height,
	                   $width, $height);
	imagejpeg($thumb, '../uploads/' . $dst, 100);


    $msg = new stdClass();
    $msg->result = $dst;
    die(json_encode($msg));

} catch (RuntimeException $e) {
	$msg = new stdClass();
	$msg->error = $e->getMessage();
	die(json_encode($msg));
}

?>