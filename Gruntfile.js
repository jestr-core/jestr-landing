var grunt = require('grunt');
var path = require('path');

grunt.loadNpmTasks('grunt-contrib-jshint');
grunt.loadNpmTasks('grunt-contrib-uglify');
grunt.loadNpmTasks('grunt-contrib-sass');
grunt.loadNpmTasks('grunt-contrib-handlebars');

grunt.initConfig({
    jshint: {
        files: ['js/*.js'],
        options: {
            ignores: ['js/*.min.js', 'js/src/tpl.js', 'js/src/plugins.js']
        }
    },
    handlebars: {
        compile: {
            options: {
                // Remove folder path name from file
                processName: function(fileName) {
                    return path.basename(fileName, '.hbs');
                },
                namespace: "Handlebars.templates",
                amd: false
            },
            files: {
                'js/src/tpl.js': 'js/src/tpl/*.hbs'
            }
        }
    },
    uglify: {
        my_target: {
            files: {
                'js/main.min.js': ['js/vendor/handlebars.runtime-v2.0.0.js',
                    'js/src/helpers.js',
                    'js/src/plugins.js',
                    'js/src/tpl.js',
                    'js/src/renderer.js',
                    'js/src/editor.js',
                    'js/src/main.js'
                ]
            }
        }
    },
    sass: {
        build: {
            files: {
                'css/main.css': 'scss/main.scss'
            }
        },
        options: {
            style: 'compressed'
        }
    }
});

grunt.registerTask('default', ['jshint', 'handlebars', 'uglify', 'sass']);